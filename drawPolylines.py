import cv2
import json
import os
import numpy as np

for i in os.listdir('cars/'):
    if i.split(".")[1] == "json":
        with open('cars/' + i, "r") as jsnF:
            d = json.load(jsnF)
            img = cv2.imread('cars/' + i.split(".")[0] + ".jpg", 1)
            output = img.copy()
            for polygon in d['shapes']:
                pts = np.array(polygon["points"], np.int32)
                pts = pts.reshape((-1, 1, 2))
                cv2.polylines(output, [pts], True, (0, 255, 255),5)
            cv2.imshow("1", output)
            cv2.waitKey(0)
            cv2.destroyAllWindows()


