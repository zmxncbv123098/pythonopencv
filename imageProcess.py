# take an image, rotate it 180 degrees, crop by N px from left&right

import cv2
import os


def viewImage(image, name_of_window):
    cv2.namedWindow(name_of_window, cv2.WINDOW_NORMAL)
    cv2.imshow(name_of_window, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def cropLeftRightByN(image, n):  # !! n < width
    cropped = image[0:height, n:width-n]
    return cropped


def rotate180(image):
    (h, w, d) = image.shape
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, 180, 1)
    rotated = cv2.warpAffine(image, M, (w, h))
    return rotated


for i in os.listdir('images/'):
    print(i)
    img = cv2.imread('images/' + i, 1)
    rot = rotate180(img)
    height, width, channels = img.shape
    while True:
        n = int(input())

        if n < width:
            break
        print("N is not valid")
    crop = cropLeftRightByN(rot, n)
    cv2.imwrite("output/OUT" + i, crop)










