import cv2
import json
import os

for i in os.listdir('planes/'):
    if i.split(".")[1] == "json":
        with open('planes/' + i, "r") as jsnF:
            d = json.load(jsnF)
            img = cv2.imread('planes/' + i.split(".")[0] + ".jpg", 1)
            output = img.copy()
            for rectangle in d["shapes"]:
                coordinates = rectangle["points"]
                cv2.rectangle(output, (int(coordinates[0][0]), int(coordinates[0][1])), (int(coordinates[1][0]), int(coordinates[1][1])), (0, 255, 255), 5)
            cv2.imshow("1", output)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
